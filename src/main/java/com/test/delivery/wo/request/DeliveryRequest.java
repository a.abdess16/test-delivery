package com.test.delivery.wo.request;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
public class DeliveryRequest {

    private LocalDate day;
    private LocalTime start;
    private LocalTime end;

}
