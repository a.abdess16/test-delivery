package com.test.delivery.wo.response;

import lombok.Data;

import java.time.LocalDate;

@Data
public class DeliveryResponse {

    private Long id;
    private LocalDate deliveryDay;
    private TimeSlotResponse timeSlot;

}
