package com.test.delivery.wo.response;

import com.test.delivery.enums.DeliveryMode;
import lombok.Data;

import java.time.LocalTime;

@Data
public class TimeSlotResponse {

    private Long id;
    private DeliveryMode deliveryMode;
    private LocalTime startTime;
    private LocalTime endTime;

}
