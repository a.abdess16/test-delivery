package com.test.delivery.repositories;

import com.test.delivery.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ClientRepo extends JpaRepository<Client, Long> {
    Optional<Client> findByUserUsername(String username);
}
