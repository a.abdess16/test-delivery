package com.test.delivery.repositories;

import com.test.delivery.enums.DeliveryMode;
import com.test.delivery.models.TimeSlot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalTime;
import java.util.Optional;

public interface TimeSlotRepo extends JpaRepository<TimeSlot, Long> {
    Optional<TimeSlot> findByStartTimeAndEndTimeAndDeliveryMode(LocalTime startTime, LocalTime endTime, DeliveryMode mode);
}
