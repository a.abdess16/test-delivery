package com.test.delivery.mappers;

import com.test.delivery.models.TimeSlot;
import com.test.delivery.wo.response.TimeSlotResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TimeSlotMapper {

    TimeSlotResponse toDto(TimeSlot s);

}
