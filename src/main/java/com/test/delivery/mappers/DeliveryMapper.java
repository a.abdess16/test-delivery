package com.test.delivery.mappers;

import com.test.delivery.models.Delivery;
import com.test.delivery.wo.response.DeliveryResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {TimeSlotMapper.class })
public interface DeliveryMapper {

    DeliveryResponse toDto(Delivery s);

}
