package com.test.delivery.controllers;

import com.test.delivery.enums.DeliveryMode;
import com.test.delivery.mappers.DeliveryMapper;
import com.test.delivery.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController("/api/client")
@RequiredArgsConstructor
public class ClientController {
    private final ClientService clientService;
    private final DeliveryMapper deliveryMapper;

    @PutMapping("delivery-mode/{mode}")
    public ResponseEntity<String> newDelivery(@RequestParam DeliveryMode mode) {
        clientService.affectDeliveryMode(mode);
        return ResponseEntity.ok("Updated");
    }
}
