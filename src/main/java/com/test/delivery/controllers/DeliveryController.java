package com.test.delivery.controllers;

import com.test.delivery.mappers.DeliveryMapper;
import com.test.delivery.models.Delivery;
import com.test.delivery.services.DeliveryService;
import com.test.delivery.wo.request.DeliveryRequest;
import com.test.delivery.wo.response.DeliveryResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/api/delivery")
@RequiredArgsConstructor
public class DeliveryController {
    private final DeliveryService deliveryService;
    private final DeliveryMapper deliveryMapper;

    @PostMapping("")
    public ResponseEntity<DeliveryResponse> newDelivery(@RequestBody DeliveryRequest request) {
        final Delivery newDelivery = deliveryService.createDelivery(request);
        return ResponseEntity.ok(deliveryMapper.toDto(newDelivery));
    }
}
