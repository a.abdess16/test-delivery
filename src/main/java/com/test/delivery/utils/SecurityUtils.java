package com.test.delivery.utils;

import com.test.delivery.exceptions.CustomException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Transactional
public interface SecurityUtils {
    static String getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        final String login = extractPrincipal(securityContext.getAuthentication());
        if (StringUtils.isBlank(login)) {
            throw new CustomException("not authentified", HttpStatus.UNAUTHORIZED);
        }

        return login;
    }

    private static String extractPrincipal(Authentication authentication) {
        if (authentication == null) {
            return null;
        } else if (authentication.getPrincipal() instanceof UserDetails springSecurityUser) {
            return springSecurityUser.getUsername();
        } else if (authentication.getPrincipal() instanceof String username) {
            return username;
        }
        return null;
    }

}
