package com.test.delivery.services;

import com.test.delivery.models.Client;
import com.test.delivery.models.Delivery;
import com.test.delivery.wo.request.DeliveryRequest;

import java.time.LocalDate;
import java.time.LocalTime;

public interface DeliveryService {
    Delivery createDelivery(DeliveryRequest deliveryRequest);
}
