package com.test.delivery.services;

import com.test.delivery.enums.DeliveryMode;
import com.test.delivery.models.TimeSlot;

import java.time.LocalTime;

public interface TimeSlotService {
    TimeSlot findTimeSlot(LocalTime startTime, LocalTime endTime, DeliveryMode mode);
}
