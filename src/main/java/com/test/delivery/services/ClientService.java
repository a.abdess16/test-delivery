package com.test.delivery.services;

import com.test.delivery.enums.DeliveryMode;
import com.test.delivery.models.Client;

import java.util.Optional;

public interface ClientService {
    Optional<Client> findById(Long id);

    void affectDeliveryMode(DeliveryMode mode);

    Client getConnectedClient();
}
