package com.test.delivery.services.impl;

import com.test.delivery.enums.DeliveryMode;
import com.test.delivery.exceptions.CustomException;
import com.test.delivery.models.Client;
import com.test.delivery.repositories.ClientRepo;
import com.test.delivery.services.ClientService;
import com.test.delivery.utils.SecurityUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClientServiceImp implements ClientService {
    private final ClientRepo clientRepo;

    @Override
    public Optional<Client> findById(Long id) {
        return clientRepo.findById(id);
    }

    @Override
    @Transactional
    public void affectDeliveryMode(DeliveryMode mode) {
        final Client client = getConnectedClient();
        client.setDeliveryMode(mode);
        clientRepo.save(client);
    }

    @Override
    public Client getConnectedClient() {
        final String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        return clientRepo.findByUserUsername(currentUserLogin)
                .orElseThrow(() -> new CustomException("client not found", HttpStatus.UNAUTHORIZED));
    }
}
