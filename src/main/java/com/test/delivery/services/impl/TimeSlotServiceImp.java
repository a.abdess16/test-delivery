package com.test.delivery.services.impl;

import com.test.delivery.enums.DeliveryMode;
import com.test.delivery.exceptions.CustomException;
import com.test.delivery.models.TimeSlot;
import com.test.delivery.repositories.TimeSlotRepo;
import com.test.delivery.services.TimeSlotService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
@Slf4j
@RequiredArgsConstructor
public class TimeSlotServiceImp implements TimeSlotService {

    private final TimeSlotRepo timeSlotRepo;

    @Override
    public TimeSlot findTimeSlot(LocalTime startTime, LocalTime endTime, DeliveryMode mode) {
        return timeSlotRepo.findByStartTimeAndEndTimeAndDeliveryMode(startTime, endTime, mode)
                .orElseThrow(() -> {
                    log.error("Time slot not found for delivery mode {}", mode.name());
                    return new CustomException("Time slot not found");
                });
    }
}
