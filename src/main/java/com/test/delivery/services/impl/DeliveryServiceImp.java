package com.test.delivery.services.impl;

import com.test.delivery.models.Client;
import com.test.delivery.models.Delivery;
import com.test.delivery.models.TimeSlot;
import com.test.delivery.repositories.DeliveryRepo;
import com.test.delivery.services.ClientService;
import com.test.delivery.services.DeliveryService;
import com.test.delivery.services.TimeSlotService;
import com.test.delivery.wo.request.DeliveryRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DeliveryServiceImp implements DeliveryService {

    private final DeliveryRepo deliveryRepo;
    private final TimeSlotService timeSlotService;
    private final ClientService clientService;

    @Override
    public Delivery createDelivery(DeliveryRequest request) {

        final Client client = clientService.getConnectedClient();
        final TimeSlot timeSlot = timeSlotService.findTimeSlot(request.getStart(), request.getEnd(), client.getDeliveryMode());
        final Delivery delivery = Delivery.builder()
                .deliveryDay(request.getDay())
                .timeSlot(timeSlot)
                .client(client)
                .build();

        return deliveryRepo.save(delivery);
    }
}
