package com.test.delivery.models;

import com.test.delivery.enums.DeliveryMode;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstname;
    private String lastname;
    @OneToOne
    private User user;
    //TODO: add Enum converter
    @Enumerated(EnumType.ORDINAL)
    DeliveryMode deliveryMode;

}
