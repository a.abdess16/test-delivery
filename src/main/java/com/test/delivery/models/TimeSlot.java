package com.test.delivery.models;

import com.test.delivery.enums.DeliveryMode;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Entity
@Data
@Table(uniqueConstraints = {
        @UniqueConstraint(name = "UniquStartAndEndAndMode", columnNames = {"startTime", "endTime", "deliveryMode"})
})
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeSlot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    DeliveryMode deliveryMode;
    private LocalTime startTime;
    private LocalTime endTime;

}
