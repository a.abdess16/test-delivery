package com.test.delivery.models;

import com.test.delivery.enums.Role;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "app_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, unique = true)
    private String username;
    @Column(nullable = false)
    private String password;
    @Column(columnDefinition = "boolean default true")
    private boolean enabled;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Role role;

}
