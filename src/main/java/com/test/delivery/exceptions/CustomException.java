package com.test.delivery.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
public class CustomException extends RuntimeException {
    private final HttpStatus httpStatus;
    public CustomException() {
        super();
        this.httpStatus = HttpStatus.PRECONDITION_REQUIRED;
    }

    public CustomException(String message) {
        super(message);
        this.httpStatus = HttpStatus.PRECONDITION_REQUIRED;
    }

    public CustomException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
