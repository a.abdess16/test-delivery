package com.test.delivery.enums;

public enum DeliveryMode {
    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP;
}
