package com.test.delivery.conf.security.auth;

import com.test.delivery.exceptions.CustomException;
import com.test.delivery.models.User;
import com.test.delivery.repositories.UserRepo;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {


    DaoAuthenticationProvider daoAuthenticationProvider;


    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    UserRepo userRepository;

    @PostConstruct
    private void postConstruct() {
        daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(new BCryptPasswordEncoder());
    }


    @Override
    public Authentication authenticate(Authentication authentication) {
        String username = authentication.getName();

        final User user = userRepository.findByUsername(username).orElseThrow(
                () -> new CustomException("Unauthorized!"));

        UserDetails userPrincipalDto = new CustomUserDetails(user);
        return daoAuthenticationProvider.authenticate(
          new UsernamePasswordAuthenticationToken(userPrincipalDto, authentication.getCredentials(), userPrincipalDto.getAuthorities())
        );
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }


}
