# Delivery test project

## Environnement

Pour exécuter l'application vous aurez besoin de :

- [JDK 21](https://www.oracle.com/java/technologies/downloads/#java21)
- [Maven 3](https://maven.apache.org)

## Exécuter l'application en local

Il existe plusieurs façons d'exécuter une application Spring Boot sur votre ordinateur local. Une solution consiste à exécuter la méthode « main » dans la classe « com.test.delivery.DeliveryApplication » à partir de votre IDE.

Vous pouvez également utiliser le plugin [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) :

```shell
mvn spring-boot:run
```

### Implémentation
Cette imlémentation du test consiste à utiliser une Base de données H2 (BD légère) qui enregistre les données dans un fichier (importé sur Gitlab).
Pour accéder à cette base de données il faut exécuter l'application en local et utiliser les coordonées ci-dessous:
####url: [h2-console](http://localhost:8080/h2-console)
####User Name: delivery
####Password:

Dû au temps restreint, je n'ai pas pu corriger un problème de connexion par l'authentification basique pour récuérer le client authentifié et l'utiliser dans les différents services.
Le comportement souhaité de l'application est d'exposer une API qui contient:
- Un WS qui affecte le mode de livraison au client authentifié:
```
curl --location --request POST 'http://localhost:8080/api/client/delivery-mode/DRIVE' \
--header 'Authorization: Basic dGVzdDp0ZXN0'
```

- Un WS qui authorise au client authentifié de créer une livraison dépendamment de son mode de livraison:

```
curl --location 'http://localhost:8080/api/delivery' \
--header 'Content-Type: application/json' \
--header 'Authorization: Basic dGVzdDp0ZXN0' \
--data '{
"day": "2024-02-07",
"start": "08:00",
"end": "08:30"
}'
```
### TODO
- Changer l'authentification basique par un JWT token
- Exposer une documentation Swagger de l'API
- Implémenter les principes HATEOAS dans votre API REST
